package dietGame.views;

import dietGame.model.vo.Player;

public class PlayerInfo {
	private Player player;
	
	public PlayerInfo(Player player) {
		this.player = player;
	}
	
	public void displayInformation() {
		System.out.println("====================================");
		System.out.println(player.getPlayerName() + "님의 현재 정보");
		System.out.println("====================================");
		System.out.println("현재 체중 : " + player.getCurrentWeight() + "kg");
		System.out.println("레벨 : " + player.getLevel());
		System.out.println("운동 실행 능력 : " + player.getCapacity() + "회");
		System.out.println("====================================");
	}
}
