package dietGame.views;

import java.util.Scanner;

import dietGame.controller.GameController;
import dietGame.model.vo.Exercise;
import dietGame.model.vo.JumpRope;
import dietGame.model.vo.Player;
import dietGame.model.vo.Squat;

public class GameMenu {
	private Player player;
	private GameController gameController;
	
	public GameMenu(String playerName, double currentWeight) {
		this.player = new Player(playerName, currentWeight);
		this.gameController = new GameController(player);
	}
	
	public void mainMenu() {
		Scanner sc = new Scanner(System.in);
		
		do {
			System.out.println(player.getPlayerName() + " (현재 체중 : " + player.getCurrentWeight() + "kg) 님의 게임 메뉴");
			System.out.println("====================================");
			System.out.println("1. 현재 상태 보기");
			System.out.println("2. 운동하기");
			System.out.println("9. 나가기");
			
			System.out.print("메뉴를 선택하세요 : ");
			int no = sc.nextInt();
			
			switch(no) {
				case 1 : gameController.displayPlayerInfo(); break;
				case 2 : choiceExerciseMenu(); break;
				case 9 : System.out.println("프로그램을 종료합니다."); return;
			}
		} while(true);
	}

	private void choiceExerciseMenu() {
		Scanner sc = new Scanner(System.in);
		
		do {
			System.out.println("====================================");
			System.out.println("즐길 운동의 종류를 고르세요.");
			System.out.println("1. 스쿼트 10회");
			System.out.println("2. 줄넘기 50회");
			System.out.println("9. 메인 메뉴로");
			System.out.print("메뉴 번호 : ");
			int no = sc.nextInt();
			
			Exercise exercise = null;
			switch(no) {
				case 1 : exercise = new Squat(10, 0.25); break;
				case 2 : exercise = new JumpRope(50, 0.5); break;
				case 9 : return;
				default : System.out.println("잘못 입력 하셨습니다. 다시 입력하세요");
			}
			
			if(exercise != null) {
				gameController.enjoy(exercise);
			}
			
		} while(true);
		
	}
}
