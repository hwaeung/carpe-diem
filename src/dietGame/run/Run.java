package dietGame.run;

import java.util.Scanner;

import dietGame.views.GameMenu;

public class Run {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.print("플레이어 이름을 입력해 주세요 : ");
		String playerName = sc.nextLine();
		System.out.print("현재 체중을 소숫점 아래 둘째자리까지 입력해 주세요 : ");
		double currentWeight = sc.nextDouble();
		
		new GameMenu(playerName, Math.round(currentWeight * 100) / 100.0).mainMenu();
	}

}
