package dietGame.controller;

import dietGame.model.vo.Exercise;
import dietGame.model.vo.Player;
import dietGame.views.PlayerInfo;

public class GameController {
	private Player player;

	public GameController(Player player) {
		this.player = player;
	}
	
	public void displayPlayerInfo() {
		new PlayerInfo(player).displayInformation();
	}
	
	public void enjoy(Exercise exercise) {
		while(exercise.isRemain()) {
			exercise.lossTimes(player.getCapacity());
			
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		checkRemain(exercise);
//		checkPlayerLevelUp();
	}

	private void checkRemain(Exercise exercise) {
		if(!exercise.isRemain()) {
			exercise.burnFat();
			decrementPlayerFat(exercise.getFat());
		}
	}
	
	private void decrementPlayerFat(double exerciseFat) {
		player.decrementFat(exerciseFat);
	}
	
//	private void checkPlayerLevelUp() {
//		while(player.isAvailableLevelUp()) {
//			player.levelUp();
//		}
//	}


}
