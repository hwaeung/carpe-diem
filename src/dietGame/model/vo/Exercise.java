package dietGame.model.vo;

public abstract class Exercise {
	private int times;
	private double fat;
	private boolean remainStatus;
	
	protected Exercise(int times, double fat) {
		this.times = times;
		this.fat = fat;
		this.remainStatus = true;
	}
	
	public int getTimes() {
		return times;
	}
	
	public double getFat() {
		return fat;
	}

	public boolean isRemain() {
		return remainStatus;
	}

	public void lossTimes(int amount) {
		if(times >= amount) {
			this.times -= amount;
			System.out.println("운동할 횟수가 " + amount + "회 줄어들었습니다.");
		} else {
			if(times != 0) {
				System.out.println("운동할 횟수가 " + times + "회 줄어들었습니다.");
			}
			
			times = 0;
			remainStatus = false;
			
			System.out.println("운동할 횟수가 0이 되어 운동을 마칩니다.");
		}
	}
	
	public abstract void burnFat();
}
