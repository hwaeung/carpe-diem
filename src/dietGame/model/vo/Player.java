package dietGame.model.vo;

public class Player {
	private String playerName;
	private double currentWeight;
	private int currentExp;
	private int requiredExp;
	private int level;
	private int capacity;

	public Player(String playerName, double currentWeight) {
		this.playerName = playerName;
		this.currentWeight = currentWeight;
		this.level = 1;
		this.capacity = 10;
	}
	
	public String getPlayerName() {
		return playerName;
	}
	
	public double getCurrentWeight() {
		return currentWeight;
	}
	
	public int getCurrentExp() {
		return currentExp;
	}

	public int getRequiredExp() {
		return requiredExp;
	}
	
	public int getLevel() {
		return level;
	}
	
	public int getCapacity() {
		return capacity;
	}
	
	public void decrementFat(double exerciseFat) {
		currentWeight -= exerciseFat;
		System.out.println("플레이어의 체중이 " + exerciseFat + "kg 감소하여 " + currentWeight + "kg이 되었습니다.");
	}
	
	public boolean isAvailableLevelUp() {
		return (currentExp - requiredExp >= 0)? true: false;
	}
	
//	public void levelUp() {
//		System.out.println("플레이어의 레벨이 +1 증가하였습니다!!!!");
//		level += 1;
//		currentExp = currentExp - requiredExp;
//		requiredExp *= 2;
//		capacity *= 1.2;
//	}
	
}
