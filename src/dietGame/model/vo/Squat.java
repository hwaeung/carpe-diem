package dietGame.model.vo;

public class Squat extends Exercise {
	
	public Squat(int times, double fat) {
		super(times, fat);
	}

	@Override
	public void burnFat() {
		System.out.println("스쿼트 운동이 하체의 체지방을 태웠습니다.");
	}
}
