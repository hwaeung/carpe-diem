package dietGame.model.vo;

public class JumpRope extends Exercise {
	
	public JumpRope(int times, double fat) {
		super(times, fat);
	}

	@Override
	public void burnFat() {
		System.out.println("줄넘기 운동이 전신의 체지방을 태웠습니다.");
	}
}
